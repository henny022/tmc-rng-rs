use actix::{
    fut, Actor, ActorContext, ActorFutureExt, Addr, AsyncContext, Context, ContextFutureSpawner,
    Handler, Message, Recipient, StreamHandler, WrapFuture,
};
use actix_files::Files;
use actix_web::{post, web, App, Error, HttpRequest, HttpResponse, HttpServer};
use actix_web_actors::ws;
use tmc_rng::droptable;

struct Server {
    pub session: Option<Recipient<StateMessage2>>,
}

#[derive(Message)]
#[rtype(result = "bool")]
struct StateMessage(droptable::State);

#[derive(Message)]
#[rtype(result = "()")]
struct StateMessage2(droptable::State);

#[derive(Message)]
#[rtype(result = "bool")]
struct Connect(Recipient<StateMessage2>);

#[derive(Message)]
#[rtype(result = "()")]
struct Disconnect;

impl Actor for Server {
    type Context = Context<Self>;
}

impl Handler<StateMessage> for Server {
    type Result = bool;

    fn handle(&mut self, msg: StateMessage, _ctx: &mut Self::Context) -> Self::Result {
        match &self.session {
            Some(session) => {
                session.do_send(StateMessage2(msg.0));
                true
            }
            None => false,
        }
    }
}

impl Handler<Connect> for Server {
    type Result = bool;

    fn handle(&mut self, msg: Connect, _ctx: &mut Self::Context) -> Self::Result {
        match self.session {
            Some(_) => false,
            None => {
                self.session = Some(msg.0);
                println!("tool connected");
                true
            }
        }
    }
}

impl Handler<Disconnect> for Server {
    type Result = ();

    fn handle(&mut self, _msg: Disconnect, _ctx: &mut Self::Context) -> Self::Result {
        self.session = None;
        println!("tool disconnected");
    }
}

struct Session {
    pub server: Addr<Server>,
}

impl Actor for Session {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.server
            .send(Connect(ctx.address().recipient()))
            .into_actor(self)
            .then(|result, _actor, ctx| {
                match result {
                    Ok(success) => {
                        if success {
                            ctx.text("session started")
                        } else {
                            ctx.close(None);
                            ctx.stop()
                        }
                    }
                    Err(_) => ctx.stop(),
                }
                fut::ready(())
            })
            .wait(ctx)
    }

    fn stopping(&mut self, _ctx: &mut Self::Context) -> actix::Running {
        self.server.do_send(Disconnect);
        actix::Running::Stop
    }
}

impl Handler<StateMessage2> for Session {
    type Result = ();

    fn handle(&mut self, msg: StateMessage2, ctx: &mut Self::Context) -> Self::Result {
        if let Ok(s) = serde_json::to_string(&msg.0) {
            ctx.text(s)
        }
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for Session {
    fn handle(&mut self, item: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        let msg = match item {
            Ok(msg) => msg,
            Err(_) => {
                ctx.stop();
                return;
            }
        };
        // todo heartbeats
        match msg {
            ws::Message::Text(_) => (),
            ws::Message::Binary(_) => (),
            ws::Message::Continuation(_) => ctx.stop(),
            ws::Message::Ping(msg) => ctx.pong(&msg),
            ws::Message::Pong(_) => (),
            ws::Message::Close(reason) => {
                ctx.close(reason);
                ctx.stop();
            }
            ws::Message::Nop => (),
        }
    }
}

#[post("/submit/state")]
async fn submit_state(
    state: web::Json<droptable::State>,
    server: web::Data<Addr<Server>>,
) -> HttpResponse {
    match server.send(StateMessage(state.0)).await {
        Ok(success) => {
            if success {
                HttpResponse::Ok().finish()
            } else {
                HttpResponse::Ok().finish()
            }
        }
        Err(_) => HttpResponse::BadRequest().finish(),
    }
}

async fn ws_route(
    request: HttpRequest,
    stream: web::Payload,
    server: web::Data<Addr<Server>>,
) -> Result<HttpResponse, Error> {
    ws::start(
        Session {
            server: server.get_ref().clone(),
        },
        &request,
        stream,
    )
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(Server { session: None }.start()))
            .service(submit_state)
            .route("/ws", web::get().to(ws_route))
            .service(Files::new("/", "./app/dist/").index_file("index.html"))
    })
    .workers(1)
    .bind("127.0.0.1:8081")?
    .run()
    .await
}
