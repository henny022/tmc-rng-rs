use std::fmt::Debug;
use std::marker::PhantomData;
use strum::IntoEnumIterator;
use yew::{html, Callback, Component, Properties};

use super::RadioComponent;

pub struct RadioGroup<T> {
    pub p: PhantomData<T>,
}

#[derive(Properties, PartialEq)]
pub struct RadioGroupProps<T: PartialEq> {
    pub selected: T,
    pub on_change: Callback<T>,
}

pub enum RadioGroupMessage<T> {
    Clicked(T),
}

impl<T: PartialEq + Debug + Copy + IntoEnumIterator + 'static> Component for RadioGroup<T> {
    type Message = RadioGroupMessage<T>;

    type Properties = RadioGroupProps<T>;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self { p: PhantomData }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <>
            {for T::iter().map(|v|html!{
                <div>
                <label>
                    <RadioComponent value={v==ctx.props().selected} on_click={ctx.link().callback(move |_|RadioGroupMessage::Clicked(v))} />
                    {format!("{:?}", v)}
                </label>
                </div>
            })}
            </>
        }
    }

    fn update(&mut self, ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            RadioGroupMessage::Clicked(v) => {
                ctx.props().on_change.emit(v);
                false
            }
        }
    }
}
