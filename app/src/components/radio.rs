use yew::html;
use yew::{Callback, Component, Properties};

#[derive(Properties, PartialEq)]
pub struct RadioProps {
    pub value: bool,
    pub on_click: Callback<()>,
}

pub struct RadioComponent {}

pub enum RadioMessage {
    Click,
}

impl Component for RadioComponent {
    type Message = RadioMessage;

    type Properties = RadioProps;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <input type="radio" checked={ctx.props().value} onclick={
                ctx.link().callback(|_|{
                    RadioMessage::Click
                })
            } />
        }
    }

    fn update(&mut self, ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            RadioMessage::Click => {
                ctx.props().on_click.emit(());
                false
            },
        }
    }
}
