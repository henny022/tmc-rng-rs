use std::{fmt::Debug, marker::PhantomData};
use yew::{html, Callback, Component, Properties};

pub struct Select<T> {
    p: PhantomData<T>,
}

#[derive(Properties, PartialEq)]
pub struct SelectProps<T: PartialEq> {
    pub selected: T,
    pub options: Vec<T>,
    pub on_change: Callback<T>,
}

pub enum SelectMessage<T> {
    Click(T),
}

impl<T: Debug + PartialEq + Copy + 'static> Component for Select<T> {
    type Message = SelectMessage<T>;

    type Properties = SelectProps<T>;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self { p: PhantomData }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <select>
                { for ctx.props().options.iter().map(|o|{
                    let o:T = *o;
                    html!{
                        <option
                          selected={o==ctx.props().selected}
                          onclick={ctx.link().callback(move |_|SelectMessage::Click(o))}
                        >
                            {format!("{:?}", o)}
                        </option>
                    }
                }) }
            </select>
        }
    }

    fn update(&mut self, ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            SelectMessage::Click(option) => ctx.props().on_change.emit(option),
        };
        false
    }
}
