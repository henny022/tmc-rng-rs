use gloo_console::log;
use web_sys::HtmlInputElement;
use yew::html::onchange::Event;
use yew::{html, TargetCast};
use yew::{Callback, Component, Properties};

#[derive(Properties, PartialEq)]
pub struct TextareaProps {
    pub value: String,
    pub on_change: Callback<String>,
}

pub struct TextareaComponent {}

pub enum TextareaMessage {
    Input(String),
}

impl Component for TextareaComponent {
    type Message = TextareaMessage;

    type Properties = TextareaProps;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <div>
                <textarea onchange={
                    ctx.link().callback(|e:Event|{
                        let target: HtmlInputElement = e.target_unchecked_into();
                        log!("onchange");
                        TextareaMessage::Input(target.value())
                    })
                } value={ctx.props().value.clone()}
                rows=26 cols=32 style="resize:none"/>
            </div>
        }
    }

    fn update(&mut self, ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            TextareaMessage::Input(input) => {
                ctx.props().on_change.emit(input);
                true
            }
        }
    }
}
