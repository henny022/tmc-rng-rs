use std::marker::PhantomData;
use std::str::FromStr;
use web_sys::HtmlInputElement;
use yew::{html, TargetCast};
use yew::{html::onchange::Event, Callback, Component, Properties};

#[derive(Properties, PartialEq)]
pub struct NumberProps<T: PartialEq> {
    pub value: T,
    pub on_change: Callback<T>,
}

pub struct NumberComponent<T> {
    p: PhantomData<T>,
}

pub enum NumberMessage {
    Input(Option<String>),
}

impl<T: PartialEq + Copy + ToString + FromStr + 'static> Component for NumberComponent<T> {
    type Message = NumberMessage;

    type Properties = NumberProps<T>;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {
            p: PhantomData::default(),
        }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <input type="number" value={ctx.props().value.to_string()} onchange={
                ctx.link().callback(|e:Event|{
                    let target: HtmlInputElement = e.target_unchecked_into();
                    NumberMessage::Input(Some(target.value()))
                })
            } />
        }
    }

    fn update(&mut self, ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            NumberMessage::Input(input) => {
                if let Some(input) = input {
                    if let Ok(value) = input.parse::<T>() {
                        ctx.props().on_change.emit(value);
                        return true;
                    }
                }
                false
            }
        }
    }
}
