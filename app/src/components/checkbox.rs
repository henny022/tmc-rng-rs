use yew::html;
use yew::{Callback, Component, Properties};

#[derive(Properties, PartialEq)]
pub struct CheckboxProps {
    pub value: bool,
    pub on_change: Callback<bool>,
}

pub struct CheckboxComponent {}

pub enum CheckboxMessage {
    Toggle,
}

impl Component for CheckboxComponent {
    type Message = CheckboxMessage;

    type Properties = CheckboxProps;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <input type="checkbox" checked={ctx.props().value} onclick={
                ctx.link().callback(|_|{
                    CheckboxMessage::Toggle
                })
            } />
        }
    }

    fn update(&mut self, ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            CheckboxMessage::Toggle => {
                ctx.props().on_change.emit(!ctx.props().value);
                false
            }
        }
    }
}
