mod components;

use crate::components::{
    CheckboxComponent, NumberComponent, RadioGroup, Select, TextareaComponent,
};
use strum::{EnumIter, IntoEnumIterator};
use tmc_rng::droptable::{self, Droptable, Item};
use tmc_rng::rng::TmcRng;
use web_sys::HtmlInputElement;
use yew::prelude::*;

#[derive(Properties, PartialEq)]
struct DroptableEntryProps {
    pub name: String,
    pub value: i16,
    pub sum: Option<u32>,
}

#[function_component]
fn DroptableEntry(props: &DroptableEntryProps) -> Html {
    html! {
        <tr>
        <td>{props.name.clone()}</td>
        <td style="width:4ch;text-align:end">{props.value}</td>
        if let Some(sum) = props.sum {
            if sum > 0{
                <td style="width:5ch;text-align:end">{format!("{:.1}%", props.value as f64 / sum as f64 * 100.0)}</td>
            }else{
                <td>{"NaN"}</td>
            }
        }
        </tr>
    }
}

#[derive(Properties, PartialEq)]
struct DroptableProps {
    pub droptable: Droptable,
    pub detailed: bool,
}

#[function_component]
fn DroptableComponent(props: &DroptableProps) -> Html {
    let sum = if props.detailed {
        Some(props.droptable.sum())
    } else {
        None
    };
    let class = if props.detailed { Some("nice") } else { None };
    html! {
        <table class={classes!(class)}>
        if props.detailed {
            <thead>
            <tr>
                <th>{"Item"}</th>
                <th>{"Weight"}</th>
                <th>{"Droprate"}</th>
            </tr>
            </thead>
        }
        <tbody>
        <DroptableEntry name="Nothing" value={props.droptable.none} sum={sum} />
        <DroptableEntry name="Green Rupee" value={props.droptable.rupee1} sum={sum} />
        <DroptableEntry name="Blue Rupee" value={props.droptable.rupee5} sum={sum} />
        <DroptableEntry name="Red Rupee" value={props.droptable.rupee20} sum={sum} />
        <DroptableEntry name="Heart" value={props.droptable.heart} sum={sum} />
        <DroptableEntry name="Fairy" value={props.droptable.fairy} sum={sum} />
        <DroptableEntry name="Bombs" value={props.droptable.bombs} sum={sum} />
        <DroptableEntry name="Arrows" value={props.droptable.arrows} sum={sum} />
        <DroptableEntry name="Shells" value={props.droptable.shells} sum={sum} />
        <DroptableEntry name="Red Kinstone" value={props.droptable.kinstone_red} sum={sum} />
        <DroptableEntry name="Blue Kinstone" value={props.droptable.kinstone_blue} sum={sum} />
        <DroptableEntry name="Green Kinstone" value={props.droptable.kinstone_green} sum={sum} />
        <DroptableEntry name="Beetle" value={props.droptable.beetle} sum={sum} />
        <DroptableEntry name="Unused" value={props.droptable.unused2} sum={sum} />
        <DroptableEntry name="Unused" value={props.droptable.unused3} sum={sum} />
        <DroptableEntry name="Unused" value={props.droptable.unused4} sum={sum} />
        </tbody>
        </table>
    }
}

#[derive(Properties, PartialEq)]
struct StateProps {
    pub state: droptable::State,
    pub on_change: Callback<droptable::State>,
}

struct StateComponent {}

enum StateMessage {
    UpdateHealth(u8),
    UpdatePicolyte(u8),
    UpdateBombs(u8),
    UpdateArrows(u8),
    UpdateRupees(u16),
    UpdateFusions(bool),
    UpdateFigurines(bool),
}

impl Component for StateComponent {
    type Message = StateMessage;

    type Properties = StateProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div>
            <h2>{"Game State"}</h2>
            <table style="border: 1px solid black">
                <tr>
                    <td>{"Area Droptable"}</td>
                    <td><DroptableComponent droptable={ctx.props().state.current_area_droptable} detailed=false /></td>
                </tr>
                <tr>
                    <td>{"Picolyte Type"}</td>
                    <td><NumberComponent<u8> value={ctx.props().state.picolyte_type} on_change={ctx.link().callback(StateMessage::UpdatePicolyte)}/></td>
                </tr>
                <tr>
                    <td>{"Health"}</td>
                    <td><NumberComponent<u8> value={ctx.props().state.health} on_change={ctx.link().callback(StateMessage::UpdateHealth)} /></td>
                </tr>
                <tr>
                    <td>{"Bombs"}</td>
                    <td><NumberComponent<u8> value={ctx.props().state.bombs} on_change={ctx.link().callback(StateMessage::UpdateBombs)} /></td>
                </tr>
                <tr>
                    <td>{"Arrows"}</td>
                    <td><NumberComponent<u8> value={ctx.props().state.arrows} on_change={ctx.link().callback(StateMessage::UpdateArrows)} /></td>
                </tr>
                <tr>
                    <td>{"Rupees"}</td>
                    <td><NumberComponent<u16> value={ctx.props().state.rupees} on_change={ctx.link().callback(StateMessage::UpdateRupees)} /></td>
                </tr>
                <tr>
                    <td>{"All Fusions"}</td>
                    <td><CheckboxComponent value={ctx.props().state.did_all_fusions} on_change={ctx.link().callback(StateMessage::UpdateFusions)} /></td>
                </tr>
                <tr>
                    <td>{"All Figurines"}</td>
                    <td><CheckboxComponent value={ctx.props().state.has_all_figurines} on_change={ctx.link().callback(StateMessage::UpdateFigurines)} /></td>
                </tr>
            </table>
            </div>
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            StateMessage::UpdateHealth(health) => {
                let mut state = ctx.props().state.clone();
                state.health = health;
                ctx.props().on_change.emit(state);
                false
            }
            StateMessage::UpdatePicolyte(value) => {
                let mut state = ctx.props().state.clone();
                state.picolyte_type = value;
                ctx.props().on_change.emit(state);
                false
            }
            StateMessage::UpdateBombs(value) => {
                let mut state = ctx.props().state.clone();
                state.bombs = value;
                ctx.props().on_change.emit(state);
                false
            }
            StateMessage::UpdateArrows(value) => {
                let mut state = ctx.props().state.clone();
                state.arrows = value;
                ctx.props().on_change.emit(state);
                false
            }
            StateMessage::UpdateRupees(value) => {
                let mut state = ctx.props().state.clone();
                state.rupees = value;
                ctx.props().on_change.emit(state);
                false
            }
            StateMessage::UpdateFusions(value) => {
                let mut state = ctx.props().state.clone();
                state.did_all_fusions = value;
                ctx.props().on_change.emit(state);
                false
            }
            StateMessage::UpdateFigurines(value) => {
                let mut state = ctx.props().state.clone();
                state.has_all_figurines = value;
                ctx.props().on_change.emit(state);
                false
            }
        }
    }
}

struct RngComponent {}

#[derive(Properties, PartialEq)]
struct RngProps {
    pub state: u32,
    pub on_next: Callback<()>,
    pub on_prev: Callback<()>,
    pub on_set: Callback<u32>,
}

enum RngMessage {
    Next,
    Prev,
    Set(String),
}

impl Component for RngComponent {
    type Message = RngMessage;

    type Properties = RngProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div>
                <h2>{"Rng State"}</h2>
                <input value={format!("{:08X}", ctx.props().state)} onchange={ctx.link().callback(|e:Event|{
                    let target: HtmlInputElement = e.target_unchecked_into();
                    RngMessage::Set(target.value())
                })} style="width:8ch" />
                <button onclick={ctx.link().callback(|_|RngMessage::Prev)}>{"prev"}</button>
                <button onclick={ctx.link().callback(|_|RngMessage::Next)}>{"next"}</button>
            </div>
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            RngMessage::Next => {
                ctx.props().on_next.emit(());
                false
            }
            RngMessage::Prev => {
                ctx.props().on_prev.emit(());
                false
            }
            RngMessage::Set(seed) => {
                if let Ok(seed) = u32::from_str_radix(&seed, 16) {
                    ctx.props().on_set.emit(seed);
                }
                false
            }
        }
    }
}

#[derive(Properties, PartialEq)]
struct PredictionLineProps {
    pub advance: u32,
    pub item: Option<Item>,
    pub seed: u32,
}

#[function_component]
fn PredictionLine(props: &PredictionLineProps) -> Html {
    html! {
        <tr>
          <td>{props.advance.to_string()}</td>
          if let Some(item) = props.item {
            <td>{item.to_string()}</td>
          }else{
            <td>{"Nothing"}</td>
          }
          <td>{format!("{:08X}", props.seed)}</td>
        </tr>
    }
}

struct PredictionComponent {
    count: u32,
    reverse: bool,
    hide_none: bool,
}

#[derive(Properties, PartialEq)]
struct PredictionProps {
    pub rng: TmcRng,
    pub droptable: Droptable,
}

enum PredictionMessage {
    ChangeCount(u32),
    Reverse(bool),
    HideNone(bool),
}

impl Component for PredictionComponent {
    type Message = PredictionMessage;

    type Properties = PredictionProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            count: 5,
            reverse: false,
            hide_none: false,
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let mut rng = ctx.props().rng.clone();
        html! {
            <div>
            <h2>{"upcoming items"}</h2>
            <table>
                <tr>
                    <td>{"count"}</td>
                    <td><NumberComponent<u32> value={self.count} on_change={ctx.link().callback(PredictionMessage::ChangeCount)}/></td>
                </tr>
                <tr>
                    <td>{"reverse"}</td>
                    <td><CheckboxComponent value={self.reverse} on_change={ctx.link().callback(PredictionMessage::Reverse)}/></td>
                </tr>
                <tr>
                    <td>{"hide none"}</td>
                    <td><CheckboxComponent value={self.hide_none} on_change={ctx.link().callback(PredictionMessage::HideNone)}/></td>
                </tr>
            </table>
            <table class="nice">
            <thead>
              <tr><th>{"advances"}</th><th>{"item"}</th><th>{"seed"}</th></tr>
            </thead>
            <tbody>
            {for (0..self.count).collect::<Vec<_>>().iter().map(|advance|{
                let seed = rng.state;
                let item = ctx.props().droptable.random_item(rng.clone());
                if self.reverse {
                    rng.prev();
                } else {
                    rng.next();
                };
                html!{
                    if self.hide_none {
                        if item.is_some() {
                            <PredictionLine {advance} {item} {seed} />
                        }
                    } else {
                        <PredictionLine {advance} {item} {seed} />
                    }
                }
            }) }
            </tbody>
            </table>
            </div>
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            PredictionMessage::ChangeCount(v) => {
                self.count = v;
                true
            }
            PredictionMessage::Reverse(v) => {
                self.reverse = v;
                true
            }
            PredictionMessage::HideNone(v) => {
                self.hide_none = v;
                true
            }
        }
    }
}

#[derive(EnumIter, Default, PartialEq, Eq, Clone, Copy, Debug)]
enum Mode {
    Object,
    #[default]
    Enemy,
    MoleMitts,
}

#[derive(Default, PartialEq, Eq, Clone, Copy)]
struct Input {
    pub mode: Mode,
    pub id: u8,
}

#[derive(EnumIter, Clone, Copy, Debug, PartialEq)]
enum EnemyId {
    /*0x00*/ Octorok,
    /*0x01*/ ChuChu,
    /*0x02*/ Leever,
    /*0x03*/ Peahat,
    /*0x04*/ Rollobite,
    /*0x05*/ Darknut,
    /*0x06*/ HangingSeed,
    /*0x07*/ Beetle,
    /*0x08*/ Keese,
    /*0x09*/ DoorMimic,
    /*0x0a*/ RockChu,
    /*0x0b*/ SpinyChu,
    /*0x0c*/ CuccoChickAggr,
    /*0x0d*/ Moldorm,
    /*0x0e*/ EnemyE,
    /*0x0f*/ Moldworm,
    /*0x10*/ Sluggula,
    /*0x11*/ Pesto,
    /*0x12*/ Puffstool,
    /*0x13*/ ChuChuBoss,
    /*0x14*/ LikeLike,
    /*0x15*/ SpearMoblin,
    /*0x16*/ BusinessScrub,
    /*0x17*/ RupeeLike,
    /*0x18*/ Madderpillar,
    /*0x19*/ WaterDrop,
    /*0x1a*/ WallMaster,
    /*0x1b*/ BombPeahat,
    /*0x1c*/ Spark,
    /*0x1d*/ Chaser,
    /*0x1e*/ SpikedBeetle,
    /*0x1f*/ SensorBladeTrap,
    /*0x20*/ Hemasaur,
    /*0x21*/ FallingBoulder,
    /*0x22*/ Bobomb,
    /*0x23*/ WallMaster2,
    /*0x24*/ Gleerok,
    /*0x25*/ VaatiEyesMacro,
    /*0x26*/ Tektite,
    /*0x27*/ WizzrobeWind,
    /*0x28*/ WizzrobeFire,
    /*0x29*/ WizzrobeIce,
    /*0x2a*/ Armos,
    /*0x2b*/ Eyegore,
    /*0x2c*/ Rope,
    /*0x2d*/ SmallPesto,
    /*0x2e*/ AcroBandit,
    /*0x2f*/ BladeTrap,
    /*0x30*/ Keaton,
    /*0x31*/ Crow,
    /*0x32*/ Mulldozer,
    /*0x33*/ Bombarossa,
    /*0x34*/ Wisp,
    /*0x35*/ SpinyBeetle,
    /*0x36*/ MazaalHead,
    /*0x37*/ MazaalMacro,
    /*0x38*/ MazaalHand,
    /*0x39*/ OctorokBoss,
    /*0x3a*/ FlyingPot,
    /*0x3b*/ Gobdo,
    /*0x3c*/ OctorockGolden,
    /*0x3d*/ TektiteGolden,
    /*0x3e*/ RopeGolden,
    /*0x3f*/ CloudPiranha,
    /*0x40*/ ScissorsBeetle,
    /*0x41*/ CuccoAggr,
    /*0x42*/ Stalfos,
    /*0x43*/ FlyingSkull,
    /*0x44*/ MazaalBracelet,
    /*0x45*/ Takkuri,
    /*0x46*/ BowMoblin,
    /*0x47*/ Lakitu,
    /*0x48*/ LakituCloud,
    /*0x49*/ TorchTrap,
    /*0x4a*/ VaatiReborn,
    /*0x4b*/ VaatiProjectile,
    /*0x4c*/ BallChainSolider,
    /*0x4d*/ Enemy4D,
    /*0x4e*/ Ghini,
    /*0x4f*/ VaatiTransfigured,
    /*0x50*/ Enemy50,
    /*0x51*/ VaatiWrath,
    /*0x52*/ VaatiArm,
    /*0x53*/ Dust,
    /*0x54*/ VaatiBall,
    /*0x55*/ Octorok2,
    /*0x56*/ Slime,
    /*0x57*/ MiniSlime,
    /*0x58*/ FireballGuy,
    /*0x59*/ MiniFireballGuy,
    /*0x5a*/ VaatiTransfiguredEye,
    /*0x5b*/ BusinessScrubPrologue,
    /*0x5c*/ GyorgFemale,
    /*0x5d*/ GyorgMale,
    /*0x5e*/ Curtain,
    /*0x5f*/ VaatiWrathEye,
    /*0x60*/ GyorgChild,
    /*0x61*/ GyorgFemaleEye,
    /*0x62*/ GyorgMaleEye,
    /*0x63*/ GyorgFemaleMouth,
    /*0x64*/ Enemy64,
    /*0x65*/ TreeItem,
    /*0x66*/ Enemy66,
}

#[derive(EnumIter, Debug, PartialEq, Clone, Copy)]
enum SpecialFxDrop {
    Bush,     // 0x11
    Rock,     // 0x12
    Pot,      // 0x13
    Fire,     // 0x14
    Grass,    // 0x10
    BluePuff, // 0x18
}

impl SpecialFxDrop {
    fn get_fx(&self) -> u8 {
        match self {
            SpecialFxDrop::Bush => 3,
            SpecialFxDrop::Rock => 4,
            SpecialFxDrop::Pot => 5,
            SpecialFxDrop::Fire => 22,
            SpecialFxDrop::Grass => 23,
            SpecialFxDrop::BluePuff => 83,
        }
    }
}

#[derive(EnumIter, Debug, PartialEq, Clone, Copy)]
enum MoleMittsDroptable {
    Normal,
    EasternHillsNorth,
}

impl MoleMittsDroptable {
    fn get_droptable(&self) -> u8 {
        match self {
            MoleMittsDroptable::Normal => 22,
            MoleMittsDroptable::EasternHillsNorth => 23,
        }
    }
}

struct InputComponent {}

#[derive(Properties, PartialEq)]
struct InputProps {
    pub input: Input,
    pub on_change: Callback<Input>,
}

enum InputMessage {
    ChangeMode(Mode),
    SelectObject(SpecialFxDrop),
    SelectEnemy(EnemyId),
    SelectMitts(MoleMittsDroptable),
}

impl Component for InputComponent {
    type Message = InputMessage;

    type Properties = InputProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <>
            <h2>{"Configuration"}</h2>
            <div style="display=flex;flex-direction:column">
                <RadioGroup<Mode> selected={ctx.props().input.mode} on_change={ctx.link().callback(InputMessage::ChangeMode)} />
                {
                    match ctx.props().input.mode {
                        Mode::Object => html!{
                            <>
                            <Select<SpecialFxDrop>
                              selected={SpecialFxDrop::Bush}
                              options={SpecialFxDrop::iter().collect::<Vec<_>>()}
                              on_change={ctx.link().callback(InputMessage::SelectObject)}
                            />
                            if ctx.props().input.id == SpecialFxDrop::BluePuff.get_fx(){
                                <b>{"not implemented"}</b>
                            }
                            </>
                        },
                        Mode::Enemy => html!{
                            <Select<EnemyId>
                              selected={EnemyId::Octorok}
                              options={EnemyId::iter().filter(|e|droptable::DEATH_FX_TABLES[*e as usize]!=0).collect::<Vec<_>>()}
                              on_change={ctx.link().callback(InputMessage::SelectEnemy)}
                            />
                        },
                        Mode::MoleMitts => html!{
                            <Select<MoleMittsDroptable>
                              selected={MoleMittsDroptable::Normal}
                              options={MoleMittsDroptable::iter().collect::<Vec<_>>()}
                              on_change={ctx.link().callback(InputMessage::SelectMitts)}
                            />
                        },
                    }
                }
            </div>
            </>
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            InputMessage::ChangeMode(mode) => {
                ctx.props().on_change.emit(Input {
                    mode: mode,
                    id: match mode {
                        Mode::Object => SpecialFxDrop::Bush.get_fx(),
                        Mode::Enemy => EnemyId::Octorok as u8,
                        Mode::MoleMitts => MoleMittsDroptable::Normal.get_droptable(),
                    },
                });
                false
            }
            InputMessage::SelectObject(drop) => {
                let mut input = ctx.props().input;
                input.id = drop.get_fx();
                ctx.props().on_change.emit(input);
                false
            }
            InputMessage::SelectEnemy(drop) => {
                let mut input = ctx.props().input;
                input.id = drop as u8;
                ctx.props().on_change.emit(input);
                false
            }
            InputMessage::SelectMitts(table) => {
                let mut input = ctx.props().input;
                input.id = table.get_droptable();
                ctx.props().on_change.emit(input);
                false
            }
        }
    }
}

enum AppMessage {
    Next,
    Prev,
    StateChange(droptable::State),
    LoadJson(String),
    ChangeInput(Input),
    UpdateDroptable,
    Seed(u32),
}

#[derive(Default)]
struct App {
    pub rng: TmcRng,
    pub state: droptable::State,
    pub droptable: Option<Droptable>,
    pub input: Input,
}

impl Component for App {
    type Message = AppMessage;

    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        let mut s = Self::default();
        s.update_droptable();
        s
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            AppMessage::Next => {
                self.rng.next();
                true
            }
            AppMessage::Prev => {
                self.rng.prev();
                true
            }
            AppMessage::StateChange(state) => {
                self.state = state;
                ctx.link().send_message(AppMessage::UpdateDroptable);
                true
            }
            AppMessage::LoadJson(json) => {
                if let Ok(state) = serde_json::from_str(&json) {
                    ctx.link().send_message(AppMessage::StateChange(state));
                }
                false
            }
            AppMessage::ChangeInput(input) => {
                self.input = input;
                ctx.link().send_message(AppMessage::UpdateDroptable);
                true
            }
            AppMessage::UpdateDroptable => {
                self.update_droptable();
                true
            }
            AppMessage::Seed(seed) => {
                self.rng.seed(seed);
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let on_next = ctx.link().callback(|_| AppMessage::Next);
        let on_prev = ctx.link().callback(|_| AppMessage::Prev);

        html! {
            <div>
                <h1>{"TMC RNG tool"}</h1>
                <div style="display:flex;flex-direction: row">
                    <div style="display:flex;flex-direction:column">
                        <RngComponent state={self.rng.state} {on_next} {on_prev} on_set={ctx.link().callback(AppMessage::Seed)} />
                        <InputComponent input={self.input} on_change={ctx.link().callback(AppMessage::ChangeInput)}/>
                        if let Some(droptable) = self.droptable {
                            <PredictionComponent rng={self.rng} droptable={droptable}/>
                        }
                    </div>
                    <div>
                        <h2>{"Final Droptable"}</h2>
                        if let Some(droptable) = self.droptable {
                            <DroptableComponent droptable={droptable} detailed=true/>
                        }else{
                            <p>{"no droptable"}</p>
                        }

                    </div>
                    <StateComponent state={self.state.clone()} on_change={
                        ctx.link().callback(AppMessage::StateChange)
                    }/>
                    <div>
                        <h2>{"JSON state"}</h2>
                        <TextareaComponent value={serde_json::to_string_pretty(&self.state).unwrap()} on_change={ctx.link().callback(AppMessage::LoadJson)}/>
                    </div>
                </div>
            </div>
        }
    }
}

impl App {
    fn update_droptable(&mut self) {
        self.droptable = match self.input.mode {
            Mode::Object => droptable::get_special_fx_droptable(self.input.id, &self.state),
            Mode::Enemy => droptable::get_death_fx_droptable(self.input.id, &self.state),
            Mode::MoleMitts => droptable::get_object_droptable(self.input.id, &self.state),
        };
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}
