use crate::droptable::Droptable;
use super::data::*;
use super::State;

pub fn apply_modifiers(droptable: &mut Droptable, state: &State) {
    if state.picolyte_type != 0 {
        *droptable += ENEMY_DROPTABLES[state.picolyte_type as usize + 6];
    }

    if state.health <= 8 {
        droptable.heart += 5;
    }
    if state.bombs == 0 {
        droptable.bombs += 3;
    }
    if state.arrows == 0 {
        droptable.arrows += 3;
    }
    if state.rupees <= 10 {
        droptable.rupee5 += 1;
    }

    if state.did_all_fusions {
        *droptable += MODIFIER_NO_SHELLS
    }
    if state.has_all_figurines {
        *droptable += MODIFIER_NO_KINSTONES
    }
}

pub fn get_special_fx_droptable(fx: u8, state: &State) -> Option<Droptable> {
    let fx = fx as usize;
    if fx >= SPECIAL_FX_TABLES.len() {
        return None;
    }
    let table = SPECIAL_FX_TABLES[fx];
    if table < 16 {
        return None;
    }
    if table > 23 {
        return None;
    }
    get_object_droptable(table, state)
}

pub fn get_object_droptable(table: u8, state: &State) -> Option<Droptable> {
    let mut droptable = Droptable::default();
    droptable += OBJECT_DROPTABLES[table as usize - 16];
    droptable += state.current_area_droptable;
    apply_modifiers(&mut droptable, state);
    Some(droptable.clamped())
}

pub fn get_death_fx_droptable(enemy: u8, state: &State) -> Option<Droptable> {
    let enemy = enemy as usize;
    if enemy > 0xf0 {
        // todo special drop tables
        return None;
    }
    if enemy >= DEATH_FX_TABLES.len() {
        return None;
    }
    let table = DEATH_FX_TABLES[enemy] as usize;
    let mut droptable = Droptable::default();
    droptable += ENEMY_DROPTABLES[table];
    apply_modifiers(&mut droptable, state);
    Some(droptable.clamped())
}
