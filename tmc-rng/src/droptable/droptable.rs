use serde::{Deserialize, Serialize};
use std::{
    cmp::max,
    ops::{Add, AddAssign, Index},
};

use crate::rng::TmcRng;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Color {
    Red,
    Blue,
    Green,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Shape {
    E,
    W,
    V,
    L,
    S,
    C,
    G,
    P,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Item {
    Rupee(u8),
    Heart,
    Fairy,
    Bombs,
    Arrows,
    Shell,
    Kinstone(Color, Option<Shape>),
    Beetle,
}

impl ToString for Item {
    fn to_string(&self) -> String {
        match self {
            Item::Rupee(count) => match count {
                1 => "Green Rupee",
                5 => "Blue Rupee",
                20 => "Red Rupee",
                50 => "Big Green Rupee",
                100 => "Big Blue Rupee",
                200 => "Big Red Rupee",
                _ => return format!("Rupee({})", count),
            },
            Item::Heart => "Heart",
            Item::Fairy => "Fairy",
            Item::Bombs => "5 Bombs",
            Item::Arrows => "5 Arrows",
            Item::Shell => "1 Mysterious Shell",
            Item::Kinstone(color, shape) => match color {
                Color::Red => match shape {
                    Some(shape) => match shape {
                        Shape::E => "Red Kinstone (E)",
                        Shape::W => "Red Kinstone (W)",
                        Shape::V => "Red Kinstone (V)",
                        _ => "Red Kinstone (illegal)",
                    },
                    None => "Red Kinstone",
                },
                Color::Blue => match shape {
                    Some(shape) => match shape {
                        Shape::L => "Blue Kinstone (L)",
                        Shape::S => "Blue Kinstone (S)",
                        _ => "Blue Kinstone (illegal)",
                    },
                    None => "Blue Kinstone",
                },
                Color::Green => match shape {
                    Some(shape) => match shape {
                        Shape::C => "Green Kinstone (C)",
                        Shape::G => "Green Kinstone (G)",
                        Shape::P => "Green Kinstone (P)",
                        _ => "Green Kinstone (illegal)",
                    },
                    None => "Green Kinstone",
                },
            },
            Item::Beetle => "Beetle",
        }
        .to_owned()
    }
}

#[derive(PartialEq, Eq, Default, Clone, Copy, Serialize, Deserialize, Debug)]
pub struct Droptable {
    pub none: i16,
    pub rupee1: i16,
    pub rupee5: i16,
    pub rupee20: i16,
    pub heart: i16,
    pub fairy: i16,
    pub bombs: i16,
    pub arrows: i16,
    pub shells: i16,
    pub kinstone_red: i16,
    pub kinstone_blue: i16,
    pub kinstone_green: i16,
    pub beetle: i16,
    pub unused2: i16,
    pub unused3: i16,
    pub unused4: i16,
}

impl Droptable {
    pub fn clamped(&self) -> Self {
        Self {
            none: max(self.none, 0),
            rupee1: max(self.rupee1, 0),
            rupee5: max(self.rupee5, 0),
            rupee20: max(self.rupee20, 0),
            heart: max(self.heart, 0),
            fairy: max(self.fairy, 0),
            bombs: max(self.bombs, 0),
            arrows: max(self.arrows, 0),
            shells: max(self.shells, 0),
            kinstone_red: max(self.kinstone_red, 0),
            kinstone_blue: max(self.kinstone_blue, 0),
            kinstone_green: max(self.kinstone_green, 0),
            beetle: max(self.beetle, 0),
            unused2: max(self.unused2, 0),
            unused3: max(self.unused3, 0),
            unused4: max(self.unused4, 0),
        }
    }

    pub fn sum(&self) -> u32 {
        (self.none as u32)
            + (self.rupee1 as u32)
            + (self.rupee5 as u32)
            + (self.rupee20 as u32)
            + (self.heart as u32)
            + (self.fairy as u32)
            + (self.bombs as u32)
            + (self.arrows as u32)
            + (self.shells as u32)
            + (self.kinstone_red as u32)
            + (self.kinstone_blue as u32)
            + (self.kinstone_green as u32)
            + (self.beetle as u32)
            + (self.unused2 as u32)
            + (self.unused3 as u32)
            + (self.unused4 as u32)
    }

    pub fn random_item(&self, mut rng: TmcRng) -> Option<Item> {
        if self.sum() == 0 {
            return None;
        }
        let rand = rng.next();
        let mut item = (rand >> 24) % 16;
        let rand = rand % self.sum();
        let mut i: u32 = 0;
        let mut j: u32 = 0;
        while i < 16 {
            j += self[item] as u32;
            if j > rand {
                break;
            }
            item += 1;
            item %= 16;
            i += 1;
        }
        match item {
            0 => None,
            1 => Some(Item::Rupee(1)),
            2 => Some(Item::Rupee(5)),
            3 => Some(Item::Rupee(20)),
            4 => Some(Item::Heart),
            5 => Some(Item::Fairy),
            6 => Some(Item::Bombs),
            7 => Some(Item::Arrows),
            8 => Some(Item::Shell),
            9 => {
                let rand = rng.next() % 0x40;
                const REDS: [Option<Shape>; 0x40] = [
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::W),
                    Some(Shape::E),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::V),
                    Some(Shape::W),
                    Some(Shape::W),
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::W),
                    Some(Shape::E),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::V),
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::W),
                    Some(Shape::E),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::V),
                    Some(Shape::W),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::V),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::W),
                    Some(Shape::W),
                    Some(Shape::E),
                    Some(Shape::V),
                    Some(Shape::E),
                    Some(Shape::V),
                    Some(Shape::W),
                    None,
                ];
                if let Some(shape) = REDS[rand as usize] {
                    Some(Item::Kinstone(Color::Red, Some(shape)))
                } else {
                    None
                }
            }
            10 => {
                let rand = rng.next() % 0x40;
                const BLUES: [Option<Shape>; 0x40] = [
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                    Some(Shape::L),
                    Some(Shape::S),
                ];
                if let Some(shape) = BLUES[rand as usize] {
                    Some(Item::Kinstone(Color::Blue, Some(shape)))
                } else {
                    None
                }
            }
            11 => {
                let rand = rng.next() % 0x40;
                const GREENS: [Option<Shape>; 0x40] = [
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::C),
                    Some(Shape::P),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::G),
                    Some(Shape::C),
                    Some(Shape::C),
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::C),
                    Some(Shape::P),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::G),
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::C),
                    Some(Shape::P),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::G),
                    Some(Shape::C),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::G),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::C),
                    Some(Shape::C),
                    Some(Shape::P),
                    Some(Shape::G),
                    Some(Shape::P),
                    Some(Shape::G),
                    Some(Shape::C),
                    None,
                ];
                if let Some(shape) = GREENS[rand as usize] {
                    Some(Item::Kinstone(Color::Green, Some(shape)))
                } else {
                    None
                }
            }
            12 => Some(Item::Beetle),
            13 => None,
            14 => None,
            15 => None,
            _ => None,
        }
    }
}

impl Index<u32> for Droptable {
    type Output = i16;

    fn index(&self, index: u32) -> &Self::Output {
        match index {
            0 => &self.none,
            1 => &self.rupee1,
            2 => &self.rupee5,
            3 => &self.rupee20,
            4 => &self.heart,
            5 => &self.fairy,
            6 => &self.bombs,
            7 => &self.arrows,
            8 => &self.shells,
            9 => &self.kinstone_red,
            10 => &self.kinstone_blue,
            11 => &self.kinstone_green,
            12 => &self.beetle,
            13 => &self.unused2,
            14 => &self.unused3,
            15 => &self.unused4,
            _ => &0,
        }
    }
}

impl Add for Droptable {
    type Output = Droptable;

    fn add(self, rhs: Self) -> Self::Output {
        Self::Output {
            none: self.none + rhs.none,
            rupee1: self.rupee1 + rhs.rupee1,
            rupee5: self.rupee5 + rhs.rupee5,
            rupee20: self.rupee20 + rhs.rupee20,
            heart: self.heart + rhs.heart,
            fairy: self.fairy + rhs.fairy,
            bombs: self.bombs + rhs.bombs,
            arrows: self.arrows + rhs.arrows,
            shells: self.shells + rhs.shells,
            kinstone_red: self.kinstone_red + rhs.kinstone_red,
            kinstone_blue: self.kinstone_blue + rhs.kinstone_blue,
            kinstone_green: self.kinstone_green + rhs.kinstone_green,
            beetle: self.beetle + rhs.beetle,
            unused2: self.unused2 + rhs.unused2,
            unused3: self.unused3 + rhs.unused3,
            unused4: self.unused4 + rhs.unused4,
        }
    }
}

impl AddAssign for Droptable {
    fn add_assign(&mut self, rhs: Self) {
        self.none += rhs.none;
        self.rupee1 += rhs.rupee1;
        self.rupee5 += rhs.rupee5;
        self.rupee20 += rhs.rupee20;
        self.heart += rhs.heart;
        self.fairy += rhs.fairy;
        self.bombs += rhs.bombs;
        self.arrows += rhs.arrows;
        self.shells += rhs.shells;
        self.kinstone_red += rhs.kinstone_red;
        self.kinstone_blue += rhs.kinstone_blue;
        self.kinstone_green += rhs.kinstone_green;
        self.beetle += rhs.beetle;
        self.unused2 += rhs.unused2;
        self.unused3 += rhs.unused3;
        self.unused4 += rhs.unused4;
    }
}
