use super::Droptable;
use serde::{Serialize, Deserialize};

#[derive(PartialEq, Eq, Default, Clone, Serialize, Deserialize, Debug)]
pub struct State {
    pub current_area_droptable: Droptable,
    pub picolyte_type: u8,
    pub health: u8,
    pub bombs: u8,
    pub arrows: u8,
    pub rupees: u16,
    pub has_all_figurines: bool,
    pub did_all_fusions: bool,
}
