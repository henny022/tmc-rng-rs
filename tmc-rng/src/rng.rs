#[derive(Clone, Copy, PartialEq, Eq)]
pub struct TmcRng {
    pub state: u32,
}

impl TmcRng {
    pub fn from_seed(seed: u32) -> Self {
        Self { state: seed }
    }

    pub fn seed(&mut self, seed: u32) {
        self.state = seed;
    }

    pub fn next(&mut self) -> u32 {
        self.state = self.state.wrapping_mul(3);
        self.state = self.state.rotate_right(13);
        self.state >> 1
    }

    pub fn prev(&mut self) -> u32 {
        self.state = self.state.rotate_left(13);
        self.state = self.state.wrapping_mul(2863311531);
        self.state >> 1
    }
}

impl Default for TmcRng {
    fn default() -> Self {
        Self::from_seed(0x01234567)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reverse() {
        let mut rng = TmcRng::from_seed(0x1234567);
        rng.next();
        assert_ne!(rng.state, 0x1234567);
        rng.prev();
        assert_eq!(rng.state, 0x1234567);
    }

    #[test]
    #[ignore = "expensive"]
    fn reverse_all() {
        for seed in 1..=u32::MAX {
            let mut rng = TmcRng::from_seed(seed);
            rng.next();
            assert_ne!(rng.state, seed);
            rng.prev();
            assert_eq!(rng.state, seed);
        }
    }

    #[test]
    fn zero() {
        let mut rng = TmcRng::from_seed(0);
        rng.next();
        assert_eq!(rng.state, 0);
        rng.prev();
        assert_eq!(rng.state, 0);
    }

    #[test]
    #[ignore = "expensive"]
    fn sequence() {
        let mut rng = TmcRng::from_seed(0x1234567);
        let mut count: usize = 0;
        loop {
            rng.next();
            count += 1;
            if rng.state == 0x1234567 {
                break;
            }
        }
        assert_eq!(count, 822119800);
    }
}
